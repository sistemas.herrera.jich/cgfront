import React, { useEffect, useContext, useState } from "react";
import { Path } from "../elements/elements";
import ContactsTop from "../components/contacts-top";
import NavBar from "../components/nav-bar";
import LatestNews from "../components/latest-news";
import Slider from "../components/slider";
import RowVertical from "../components/row-vertical";
import Footer from "../components/footer";
import AppContext from "../context/context";
import back from "../assets/images/arriba.jpg";
import back2 from "../assets/images/abajo.jpg";
import Loader from "react-loader";
import "react-animated-slider/build/horizontal.css";

const IndexPage = () => {
  const { publicaciones, noticias, articulos, categorias } =
    useContext(AppContext);
  const { publicacion, setPublicaciones } = publicaciones;
  const { noticia, setNoticias } = noticias;
  const { articulo, setArticulos } = articulos;
  const { categoria, setCategorias, setCategoriasForce } = categorias;
  useEffect(async () => {
    await setPublicaciones(`${Path}/publicaciones?offset=0&limit=5`);
    await setNoticias(`${Path}/noticias?offset=0&limit=10`);
    await setArticulos(`${Path}/destacados`);
    await setCategorias(`${Path}/noticias?categorias=Accion`);
  }, []);

  return (
    <div className="">
      <img className="nk-page-background-top" src={back} alt="" />
      {publicacion != null && noticia != null && articulo != null ? (
        <div className="avers">
          <img className="nk-page-background-bottom" src={back2} alt="" />
          <header className="nk-header nk-header-opaque">
            <ContactsTop />

            <NavBar />
          </header>
          <div className="nk-main">
            <div className="nk-gap-1"></div>
            <Slider articulo={articulo} />
            <div className="nk-gap-2"></div>
            <div className="container">
              <h3 className="nk-decorated-h-2">
                <span>
                  <span className="text-main-1">Últimas</span> Noticias
                </span>
              </h3>
            </div>
            <LatestNews publicacion={noticia.noticias} />
            <div className="nk-gap-2"></div>
            {categoria != null ? (
              <RowVertical
                noticia={categoria.noticias}
                setNoticias={setCategorias}
                setNoticiasForce={setCategoriasForce}
                publicacion={publicacion}
              />
            ) : (
              <div className="LoaderCentrado">
                <Loader
                  loaded={false}
                  lines={13}
                  length={20}
                  width={5}
                  radius={20}
                  corners={1}
                  rotate={0}
                  direction={1}
                  color="white"
                  speed={1}
                  trail={60}
                  shadow={false}
                  hwaccel={false}
                  className="spinner"
                  zIndex={2e9}
                  top="50%"
                  left="50%"
                  scale={0.7}
                  loadedClassName="loadedContent"
                />
              </div>
            )}

            <div className="nk-gap-4"></div>
            <Footer />

            {/*     <div
              className="nk-modal modal fade"
              id="modalSearch"
              tabIndex="-1"
              role="dialog"
              aria-hidden="true"
            >
            <div className="modal-dialog modal-sm" role="document">
                <div className="modal-content">
                  <div className="modal-body">
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <span className="ion-android-close"></span>
                    </button>

                    <h4 className="mb-0">Search</h4>

                    <div className="nk-gap-1"></div>
                    <form action="#" className="nk-form nk-form-style-1">
                      <input
                        type="text"
                        value=""
                        name="search"
                        className="form-control"
                        placeholder="Type something and press Enter"
                        autoFocus
                      />
                    </form>
                  </div>
                </div>
              </div>
            </div> */}
            <div
              className="nk-modal modal fade"
              id="modalLogin"
              tabIndex="-1"
              role="dialog"
              aria-hidden="true"
            >
              <div className="modal-dialog modal-sm" role="document">
                <div className="modal-content">
                  <div className="modal-body">
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <span className="ion-android-close"></span>
                    </button>

                    <h4 className="mb-0">
                      <span className="text-main-1">Sign</span> In
                    </h4>

                    <div className="nk-gap-1"></div>
                    {/* <form action="#" className="nk-form text-white">
                      <div className="row vertical-gap">
                        <div className="col-md-6">
                          Use email and password:
                          <div className="nk-gap"></div>
                          <input
                            type="email"
                            value=""
                            name="email"
                            className="form-control"
                            placeholder="Email"
                          />
                          <div className="nk-gap"></div>
                          <input
                            type="password"
                            value=""
                            name="password"
                            className="requi form-control"
                            placeholder="Password"
                          />
                        </div>
                        <div className="col-md-6">
                          Or social account:
                          <div className="nk-gap"></div>
                          <ul className="nk-social-links-2">
                            <li>
                              <a className="nk-social-facebook" href="#">
                                <span className="fab fa-facebook"></span>
                              </a>
                            </li>
                            <li>
                              <a className="nk-social-google-plus" href="#">
                                <span className="fab fa-google-plus"></span>
                              </a>
                            </li>
                            <li>
                              <a className="nk-social-twitter" href="#">
                                <span className="fab fa-twitter"></span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>

                      <div className="nk-gap-1"></div>
                      <div className="row vertical-gap">
                        <div className="col-md-6">
                          <a
                            href="#"
                            className="
                      nk-btn nk-btn-rounded nk-btn-color-white nk-btn-block
                    "
                          >
                            Sign In
                          </a>
                        </div>
                        <div className="col-md-6">
                          <div className="mnt-5">
                            <small>
                              <a href="#">Forgot your password?</a>
                            </small>
                          </div>
                          <div className="mnt-5">
                            <small>
                              <a href="#">Not a member? Sign up</a>
                            </small>
                          </div>
                        </div>
                      </div>
                    </form> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="LoaderCentrado">
          <Loader
            loaded={false}
            lines={13}
            length={20}
            width={5}
            radius={20}
            corners={1}
            rotate={0}
            direction={1}
            color="white"
            speed={1}
            trail={60}
            shadow={false}
            hwaccel={false}
            className="spinner"
            zIndex={2e9}
            top="50%"
            left="50%"
            scale={0.7}
            loadedClassName="loadedContent"
          />
        </div>
      )}
    </div>
  );
};
export default IndexPage;
