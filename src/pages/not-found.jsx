import React from "react";
import back from "../assets/images/arriba.jpg";

function NotFound() {
  return (
    <div className="nk-main">
      <div className="nk-fullscreen-block">
        <div className="nk-fullscreen-block-top">
          <div className="text-center">
            <div className="nk-gap-4"></div>
            <a href="/">
              <img className="nk-page-background-top" src={back} alt="" />
            </a>
            <div className="nk-gap-2"></div>
          </div>
        </div>
        <div className="nk-fullscreen-block-middle">
          <div className="container text-center">
            <div className="row">
              <div className="col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                <h1 className="text-main-1" style={{ fontSize: "150px" }}>
                  404
                </h1>

                <div className="nk-gap"></div>
                <h2 className="h4">No de encontró </h2>

                <div>
                  Parece que la página que buscas no existe... te gustaría
                  volver a la página de inicio?
                </div>
                <div className="nk-gap-3"></div>

                <a
                  href="/"
                  className="nk-btn nk-btn-rounded nk-btn-color-white"
                >
                  Regresar a la página de inicio
                </a>
              </div>
            </div>
            <div className="nk-gap-3"></div>
          </div>
        </div>
        <div className="nk-fullscreen-block-bottom">
          <div className="nk-gap-2"></div>
          <ul className="nk-social-links-2 nk-social-links-center">
            <li>
              <a
                className="nk-social-twitch"
                href="https://www.twitch.tv/centralgamerla\"
              >
                <span className="fab fa-twitch"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-instagram"
                href="https://www.instagram.com/centralgamerla/"
              >
                <span className="fab fa-instagram"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-facebook"
                href="https://www.facebook.com/CentralGamerLA"
              >
                <span className="fab fa-facebook"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-google-plus"
                href="https://t.me/joinchat/HegIn-DUZaQ1ZmJh"
              >
                <span className="fab fa-telegram"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-youtube"
                href="https://www.youtube.com/c/CentralGamerLA"
              >
                <span className="fab fa-youtube"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-twitter"
                href="https://twitter.com/centralgamerla
"
                target="_blank"
              >
                <span className="fab fa-twitter"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-pinterest"
                href="https://discord.gg/ek7ec3wh"
              >
                <span className="fab fa-discord"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-rss"
                href="https://www.tiktok.com/@centralgamerla?lang=es"
              >
                <span className="fab fa-tiktok"></span>
              </a>
            </li>
            {/* --> */}
          </ul>
          <div className="nk-gap-2"></div>
        </div>
      </div>
    </div>
  );
}

export default NotFound;
