import React, { useEffect, useContext, useState } from "react";
import { Link, useParams, useHistory } from "react-router-dom";
import AppContext from "../context/context";
import ContactsTop from "../components/contacts-top";
import NavBar from "../components/nav-bar";
import TabbedNews from "../components/tabbed-news";
import LatestPictures from "../components/latest-pictures";
import LatestScreenShot from "../components/latest-screenshot";
import LatestVideo from "../components/latest-video";
import PostsNew from "../components/posts-new";
import SocialMedia from "../components/social-media";
import Recent from "../components/recent";
import Footer from "../components/footer";
import back from "../assets/images/arriba.jpg";
import back2 from "../assets/images/abajo.jpg";
import Loader from "react-loader";
import { linkVideos } from "../elements/video";
import { Path } from "../elements/elements";

function News() {
  const { publicaciones, noticias, paginas } = useContext(AppContext);
  const { publicacion, setPublicaciones } = publicaciones;
  const { noticia, setNoticias, setNoticiasForce } = noticias;
  const { pagina, setPaginas } = paginas;
  const { id } = useParams();
  const history = useHistory();
  useEffect(async () => {
    await setPublicaciones(
      `${Path}/noticias?offset=${Number(id) - 1}&limit=10`
    );
    await setPaginas(`${Path}/noticias`);
    await setNoticias(`${Path}/noticias?categorias=Accion`);
  }, []);
  return (
    <div>
      <img className="nk-page-background-top" src={back} alt="" />

      {noticia != null ? (
        <div>
          <img className="nk-page-background-bottom" src={back2} alt="" />
          <header className="nk-header nk-header-opaque">
            <ContactsTop />
            <NavBar />

            <div className="nk-gap-4"></div>
          </header>
          <div className="container">
            <div className="row vertical-gap">
              <div className="nk-gap-1"></div>
              <div className="container">
                <ul className="nk-breadcrumbs">
                  <li>
                    <a href="index.html">INICIO</a>
                  </li>

                  <li>
                    <span className="fa fa-angle-right"></span>
                  </li>

                  <li>
                    <span>NOTICIAS</span>
                  </li>
                </ul>
              </div>
              <div className="col-lg-8">
                <TabbedNews
                  noticia={noticia.noticias}
                  setNoticias={setNoticias}
                  setNoticiasForce={setNoticiasForce}
                  publicacion={publicacion.noticias}
                />
                <h3 className="nk-decorated-h-2">
                  <span>
                    <span className="text-main-1">TODAS LAS</span> NOTICIAS
                  </span>
                </h3>
                <PostsNew
                  noticias={publicacion.noticias}
                  setPublicaciones={setPublicaciones}
                  pagina={pagina.noticias}
                />
              </div>
              <div className="col-lg-4">
                {/*
                START: Sidebar

                Additional Classes:
                    .nk-sidebar-left
                    .nk-sidebar-right
                    .nk-sidebar-sticky
            */}

                <aside className="nk-sidebar nk-sidebar-right nk-sidebar-sticky">
                  <SocialMedia />
                  <div className="nk-gap-2"></div>
                  <LatestVideo embedId={linkVideos} />
                  <div className="nk-gap-2"></div>
                  <Recent />
                </aside>
                {/* END: Sidebar */}
              </div>
            </div>
          </div>
          <Footer />
        </div>
      ) : (
        <div className="LoaderCentrado">
          <Loader
            loaded={false}
            lines={13}
            length={20}
            width={5}
            radius={20}
            corners={1}
            rotate={0}
            direction={1}
            color="white"
            speed={1}
            trail={60}
            shadow={false}
            hwaccel={false}
            className="spinner"
            zIndex={2e9}
            top="50%"
            left="50%"
            scale={0.7}
            loadedClassName="loadedContent"
          />
        </div>
      )}
    </div>
  );
}

export default News;
