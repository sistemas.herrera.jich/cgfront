import React, { useEffect, useState, useContext } from "react";
import AppContext from "../context/context";
import { formatearFecha } from "../elements/elements";
import { useParams } from "react-router-dom";
import ContactsTop from "../components/contacts-top";
import NavBar from "../components/nav-bar";
import TabbedNews from "../components/tabbed-news";
import LatestPictures from "../components/latest-pictures";
import LatestScreenShot from "../components/latest-screenshot";
import LatestVideo from "../components/latest-video";
import PostsNew from "../components/posts-new";
import SocialMedia from "../components/social-media";
import Recent from "../components/recent";
import Footer from "../components/footer";
import SimilarPost from "../components/similar-post";
import back from "../assets/images/arriba.jpg";
import back2 from "../assets/images/abajo.jpg";
import Loader from "react-loader";
import { linkVideos } from "../elements/video";
import { Path } from "../elements/elements";

function article() {
  const [tipo, setTipo] = useState();
  let { tipoP, id } = useParams();
  const { articulos, noticias, publicaciones } = useContext(AppContext);
  const { articulo, setArticulos } = articulos;
  const { noticia, setNoticias } = noticias;
  const { publicacion, setPublicaciones } = publicaciones;

  useEffect(async () => {
    await setArticulos(
      `${Path}/filtro?colecciones=noticias,publicaciones&_id=${id}&normalizar=true`
    );

    setNoticias(`${Path}/noticias?offset=0&limit=2`);
  }, []);
  return (
    <div>
      {tipo}
      <img className="nk-page-background-top" src={back} alt="" />
      {articulo != null && noticia != null ? (
        <div>
          <img className="nk-page-background-bottom" src={back2} alt="" />
          <header className="nk-header nk-header-opaque">
            <ContactsTop />
            <NavBar />

            <div className="nk-gap-1"></div>
          </header>
          <div className="">
            <div className="nk-main">
              {/* START: Breadcrumbs */}
              <div className="container">
                <ul className="nk-breadcrumbs">
                  <li>
                    <a href="/">INICIO</a>
                  </li>

                  <li>
                    <span className="fa fa-angle-right"></span>
                  </li>

                  <li>
                    <a href="#">Articulo</a>
                  </li>
                  <div className="nk-gap-1"></div>
                  {/* 
                  <li>
                    <span className="fa fa-angle-right"></span>
                  </li> */}

                  <li>
                    <span>{articulo != null ? articulo.titulo : ""}</span>
                  </li>
                </ul>
              </div>
              <div className="nk-gap-1"></div>
              {/* END: Breadcrumbs */}

              <div className="container">
                <div className="row vertical-gap">
                  <div className="col-lg-8">
                    {/* START: Post */}
                    <div className="nk-blog-post nk-blog-post-single">
                      {/* START: Post Text */}
                      <div className="nk-post-text mt-0">
                        <div className="nk-post-img">
                          {tipoP == "noticias" ? (
                            <img
                              src={`${Path}/noticias/imagen/${articulo.noticias[0]._id}`}
                            />
                          ) : (
                            <img
                              src={`${Path}/publicaciones/imagen/${articulo.publicaciones[0]._id}`}
                            />
                          )}
                        </div>
                        <div className="nk-gap-1"></div>
                        <h1 className="nk-post-title h4">
                          {tipoP == "noticias"
                            ? articulo.noticias[0].titulo
                            : articulo.publicaciones[0].titulo}
                        </h1>
                        <div className="nk-post-by">
                          {tipoP == "noticias" ? (
                            <img
                              src={`${Path}/noticias/imagen/${articulo.noticias[0]._id}`}
                              alt="Witch Murder"
                              className="rounded-circle"
                              width="35"
                            />
                          ) : (
                            <img
                              src={`${Path}/publicaciones/imagen/${articulo.publicaciones[0]._id}`}
                              alt="Witch Murder"
                              className="rounded-circle"
                              width="35"
                            />
                          )}
                          by{" "}
                          <a href="#">
                            {tipoP == "noticias" && articulo.noticias != null
                              ? articulo.noticias[0].usuario.nombre
                              : ""}
                            {tipoP == "publicaciones" &&
                            articulo.publicaciones != null
                              ? articulo.publicaciones[0].usuario.nombre
                              : ""}
                          </a>{" "}
                          {tipoP == "noticias"
                            ? formatearFecha(articulo.noticias[0].created_at)
                            : formatearFecha(
                                articulo.publicaciones[0].created_at
                              )}
                          <div className="nk-post-categories">
                            {articulo.noticias[0] &&
                            articulo.noticias[0].categorias != null &&
                            tipoP == "noticias"
                              ? articulo.noticias[0].categorias.map((item) => (
                                  <div key={item._id}>
                                    <span className="bg-main-1">
                                      {tipoP == "noticias"
                                        ? item.nombre
                                        : item.nombre}
                                    </span>
                                  </div>
                                ))
                              : ""}
                          </div>
                        </div>
                        <div className="nk-gap"></div>
                        <div style={{ minHeight: "80vh" }}>
                          {tipoP == "noticias" ? (
                            <p
                              dangerouslySetInnerHTML={{
                                __html: articulo.noticias[0].descripcion,
                              }}
                            ></p>
                          ) : (
                            <p
                              dangerouslySetInnerHTML={{
                                __html: articulo.publicaciones[0].descripcion,
                              }}
                            ></p>
                          )}
                        </div>

                        <div className="nk-gap"></div>
                        {/* <blockquote className="nk-blockquote">
                        <div className="nk-blockquote-icon">
                          <span>"</span>
                        </div>
                        <div className="nk-blockquote-content">
                          Just then her head struck against the roof of the
                          hall: in fact she was now more than nine feet high,
                          and she at once took up the little golden key and
                          hurried off to the garden door. As if she had known
                          them all her life. Indeed, she had quite a long
                          argument with the Lory.
                        </div>
                        <div className="nk-blockquote-author">
                          <span>Samuel Marlow</span>
                        </div>
                      </blockquote>

                      <div className="nk-gap"></div>

                      <img
                        className="float-left mt-0"
                        src="assets/images/post-inner-img.jpg"
                        alt=""
                      />
                      <h3 className="h4">
                        Now the races of these two have been
                      </h3>
                      
                      <div className="nk-gap"></div>
                      <div
                        className="nk-plain-video"
                        data-video="https://www.youtube.com/watch?v=6cXyQg_5uoc"
                      ></div>
                    
                      <div className="nk-gap-2"></div>
                      <p>
                        This sounded nonsense to Alice, so she said nothing, but
                        set off at once toward the Red Queen. To her surprise,
                        she lost sight of her in a moment, and found herself
                        walking in at the front-door again. For some minutes
                        Alice stood without speaking, looking out in all
                        directions over the country - and a most curious country
                        it was.
                      </p> */}

                        <div className="nk-post-share">
                          <span className="h5">Share Article:</span>

                          <ul className="nk-social-links-2">
                            <li>
                              <span
                                className="nk-social-facebook"
                                title="Share page on Facebook"
                                data-share="facebook"
                              >
                                <span className="fab fa-facebook"></span>
                              </span>
                            </li>
                            <li>
                              <span
                                className="nk-social-google-plus"
                                title="Share page on Google+"
                                data-share="google-plus"
                              >
                                <span className="fab fa-google-plus"></span>
                              </span>
                            </li>
                            <li>
                              <span
                                className="nk-social-twitter"
                                title="Share page on Twitter"
                                data-share="twitter"
                              >
                                <span className="fab fa-twitter"></span>
                              </span>
                            </li>
                            <li>
                              <span
                                className="nk-social-pinterest"
                                title="Share page on Pinterest"
                                data-share="pinterest"
                              >
                                <span className="fab fa-pinterest-p"></span>
                              </span>
                            </li>

                            {/* Additional Share Buttons
                                <li><span className="nk-social-linkedin" title="Share page on LinkedIn" data-share="linkedin"><span className="fab fa-linkedin"></span></span></li>
                                <li><span className="nk-social-vk" title="Share page on VK" data-share="vk"><span className="fab fa-vk"></span></span></li>
                            */}
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-4">
                    {/*
                </div>
                START: Sidebar

                Additional Classes:
                    .nk-sidebar-left
                    .nk-sidebar-right
                    .nk-sidebar-sticky
            */}

                    <aside className="nk-sidebar nk-sidebar-right nk-sidebar-sticky">
                      <SocialMedia />
                      <div className="nk-gap-2"></div>
                      <LatestVideo embedId={linkVideos} />
                      <div className="nk-gap-2"></div>
                      <Recent />
                    </aside>
                    {/* END: Sidebar */}
                  </div>
                  <SimilarPost noticia={noticia.noticias} />
                </div>
              </div>
            </div>
          </div>
          <Footer />
        </div>
      ) : (
        <div className="LoaderCentrado">
          <Loader
            loaded={false}
            lines={13}
            length={20}
            width={5}
            radius={20}
            corners={1}
            rotate={0}
            direction={1}
            color="white"
            speed={1}
            trail={60}
            shadow={false}
            hwaccel={false}
            className="spinner"
            zIndex={2e9}
            top="50%"
            left="50%"
            scale={0.7}
            loadedClassName="loadedContent"
          />
        </div>
      )}
    </div>
  );
}

export default article;
