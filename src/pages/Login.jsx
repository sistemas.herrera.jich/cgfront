import React, { useEffect, useRef, useContext } from "react";
import axios from "axios";
import contextValue from "../context/context";
import "../assets/css/Login.css";
const Login = ({ history }) => {
  const form = useRef(null);
  const [auth, setAuth] = useContext(contextValue);

  const handleSubmit = () => {
    const formData = new FormData(form.current);
    try {
      axios
        .post("http://localhost:3000/login", {
          user: formData.get("user"),
          password: formData.get("password"),
        })
        .then((res) => {
          setAuth({
            user: res.data.auth.user,
            token: res.data.token,
          });
          history.push("/inicio");
          console.log("la res", auth);
        });
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <div className="login">
      <div className="centrado">
        <div className="logoLogin">
          <h1>LOGO</h1>
        </div>
        <div className="inputLogin">
          <form ref={form}>
            <label>
              <h4>USERNAME</h4>
              <input
                name="user"
                className="formData"
                type="text"
                placeholder="username"
              />
            </label>
            <label>
              <h4>PASSWORD</h4>
              <input
                name="password"
                className="formData"
                type="text"
                placeholder="password"
              />
            </label>
          </form>
          <label>
            <button className="formData" type="button" onClick={handleSubmit}>
              inciar
            </button>
          </label>
        </div>
      </div>
    </div>
  );
};

export default Login;
