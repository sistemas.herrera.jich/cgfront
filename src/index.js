import React from "react";
import ReactDOM from "react-dom";
import App from "./routes/AppRoutes";
import "../src/assets/js/goodgames";

ReactDOM.render(<App />, document.getElementById("app"));
