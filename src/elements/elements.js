export const formatearFecha = (value) => {
  let fechaFragment = value.split("/");
  let mes = fechaFragment[0];
  let dia = fechaFragment[1];
  let anio = fechaFragment[2];
  return `${dia}/${mes}/${anio}`;
};
export const imagePathNoticias =
  "https://cgamer.herokuapp.com/noticias/imagen/";
export const imagePathPublicaciones =
  "https://cgamer.herokuapp.com/publicaciones/imagen/";
export const imagePathDestacados =
  "https://cgamer.herokuapp.com/destacados/imagen/";
export const Path = "https://cgamer.herokuapp.com";
