import React, { useState } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import useInitialState from "../hooks/useInitialState";
import AppContext from "../context/context";
import NavBar from "../components/nav-bar";
import IndexPage from "../pages/indexPage";
import News from "../pages/news";
import Article from "../pages/article";
import NotFound from "../pages/not-found";
import "../assets/vendor/bootstrap/dist/css/bootstrap.min.css";
import "../assets/vendor/popper.js/dist/umd/popper.min.js";
import "../assets/css/goodgames.min.css";
import "../assets/css/goodgames.css";
import "../assets/css/custom.css";

const RouteApp = () => {
  const [auth, setAuth] = useState({});
  const [publicacion, setPublicaciones, setPublicacionesForce] =
    useInitialState();
  const [noticia, setNoticias, setNoticiasForce] = useInitialState();
  const [articulo, setArticulos, setArticuloForce] = useInitialState();
  const [pagina, setPaginas] = useInitialState();
  const [categoria, setCategorias, setCategoriasForce] = useInitialState();

  const State = {
    auth: { auth, setAuth },
    publicaciones: { publicacion, setPublicaciones, setPublicacionesForce },
    noticias: { noticia, setNoticias, setNoticiasForce },
    articulos: { articulo, setArticulos, setArticuloForce },
    paginas: { pagina, setPaginas },
    categorias: { categoria, setCategorias, setCategoriasForce },
  };
  return (
    <AppContext.Provider value={State}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={IndexPage} />
          <Route exact path="/noticias/:id" component={News} />
          <Route exact path="/articulo/:tipoP/:id" component={Article} />
          <Route path="*" exact={true} component={NotFound} />
        </Switch>
      </BrowserRouter>
    </AppContext.Provider>
  );
};

export default RouteApp;
