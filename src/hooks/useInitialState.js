import React, { useState, useEffect } from "react";
import initialState from "../initialState";
import axios from "axios";

function useInitialState(path) {
  const [state, setState] = useState();

  async function getStateFromPath(path) {
    return new Promise(async (resolve, reject) => {
      if (!path) return reject("no hay ningun path");
      try {
        const response = await axios.get(path, {});
        setState(response.data);
        console.log("la data esta arriba");

        return resolve(true);
      } catch (e) {
        console.log(e);
      }
    });
  }
  return [state, getStateFromPath, setState];
}

export default useInitialState;
