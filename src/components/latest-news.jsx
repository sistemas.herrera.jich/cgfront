import React, { useState } from "react";
import { Link } from "react-router-dom";
import img from "../assets/images/post-1.jpg";
import { formatearFecha, imagePathNoticias } from "../elements/elements";

function LatestNews({ publicacion }) {
  const [activo, setActivo] = useState(publicacion[0]);
  return (
    <div className="container">
      <div className="nk-gap"></div>
      <div className="nk-news-box">
        <div className="nk-news-box-list">
          <div className="nano">
            <div className="nano-content">
              {publicacion != null
                ? publicacion.map((item) => (
                    <div
                      onClick={() => {
                        setActivo(item);
                      }}
                      className={
                        activo._id == item._id
                          ? "nk-news-box-item nk-news-box-item-active"
                          : "nk-news-box-item"
                      }
                      key={item._id}
                    >
                      <div className="nk-news-box-item-img">
                        <img src={`${imagePathNoticias}${item._id}`} />
                      </div>
                      <img
                        src={`${imagePathNoticias}${item._id}`}
                        className="nk-news-box-item-full-img"
                      />
                      <h3 className="nk-news-box-item-title">{item.titulo}</h3>

                      <span className="nk-news-box-item-categories">
                        <span className="bg-main-4">
                          {item.categorias[0] != null
                            ? item.categorias[0].nombre
                            : "undefined"}
                        </span>
                      </span>

                      <div className="nk-news-box-item-text">
                        <p>{item.descripcionCorta}</p>
                      </div>

                      <Link
                        className="nk-news-box-item-url"
                        to={{
                          pathname: `/articulo/noticias/${item._id}`,
                        }}
                      >
                        LEER MÁS
                      </Link>

                      <div className="nk-news-box-item-date">
                        <span className="fa fa-calendar"></span>{" "}
                        {formatearFecha(item.created_at)}
                      </div>
                    </div>
                  ))
                : ""}
            </div>
            {/* <!-- END: Post --> */}
          </div>
        </div>
        <div className="nk-news-box-each-info">
          <div className="nano">
            {activo != null ? (
              <div className="nano-content">
                <div className="nk-news-box-item-image">
                  <img src={`${imagePathNoticias}${activo._id}`} />
                  <span className="nk-news-box-item-categories">
                    <span className="bg-main-4">
                      {activo.categorias[0] != null
                        ? activo.categorias[0].nombre
                        : "undefined"}
                    </span>
                  </span>
                </div>
                <h3 className="nk-news-box-item-title">{activo.titulo}</h3>
                <div className="nk-news-box-item-text">
                  <p>{activo.descripcionCorta}</p>
                </div>
                <a
                  href={`/articulo/noticias/${activo._id}`}
                  className="nk-news-box-item-more"
                >
                  LEER MÁS
                </a>
                <div className="nk-news-box-item-date">
                  <span className="fa fa-calendar"></span>{" "}
                  {formatearFecha(activo.created_at)}
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default LatestNews;
