import React from "react";

function ContactsTop() {
  return (
    <div className="container">
      <div className="nk-contacts-top">
        <div className="row">
          <div className="col-9 nk-contacts-left">
            <ul className="nk-social-links">
              <li>
                <a
                  className="nk-social-twitch"
                  href="https://www.twitch.tv/centralgamerla\"
                >
                  <span className="fab fa-twitch"></span>
                </a>
              </li>
              <li>
                <a
                  className="nk-social-instagram"
                  href="https://www.instagram.com/centralgamerla/"
                >
                  <span className="fab fa-instagram"></span>
                </a>
              </li>
              <li>
                <a
                  className="nk-social-facebook"
                  href="https://www.facebook.com/CentralGamerLA"
                >
                  <span className="fab fa-facebook"></span>
                </a>
              </li>
              <li>
                <a
                  className="nk-social-google-plus"
                  href="https://t.me/joinchat/HegIn-DUZaQ1ZmJh"
                >
                  <span className="fab fa-telegram"></span>
                </a>
              </li>
              <li>
                <a
                  className="nk-social-youtube"
                  href="https://www.youtube.com/c/CentralGamerLA"
                >
                  <span className="fab fa-youtube"></span>
                </a>
              </li>
              <li>
                <a
                  className="nk-social-twitter"
                  href="https://twitter.com/centralgamerla
"
                  target="_blank"
                >
                  <span className="fab fa-twitter"></span>
                </a>
              </li>
              <li>
                <a
                  className="nk-social-pinterest"
                  href="https://discord.gg/ek7ec3wh"
                >
                  <span className="fab fa-discord"></span>
                </a>
              </li>
              <li>
                <a
                  className="nk-social-rss"
                  href="https://www.tiktok.com/@centralgamerla?lang=es"
                >
                  <span className="fab fa-tiktok"></span>
                </a>
              </li>

              {/* <!-- Additional Social Buttons 
                    <li><a className="nk-social-behance" href="#"><span className="fab fa-behance"></span></a></li>
                    <li><a className="nk-social-bitbucket" href="#"><span className="fab fa-bitbucket"></span></a></li>
                    <li><a className="nk-social-dropbox" href="#"><span className="fab fa-dropbox"></span></a></li>
                    <li><a className="nk-social-dribbble" href="#"><span className="fab fa-dribbble"></span></a></li>
                    <li><a className="nk-social-deviantart" href="#"><span className="fab fa-deviantart"></span></a></li>
                    <li><a className="nk-social-flickr" href="#"><span className="fab fa-flickr"></span></a></li>
                    <li><a className="nk-social-foursquare" href="#"><span className="fab fa-foursquare"></span></a></li>
                    <li><a className="nk-social-github" href="#"><span className="fab fa-github"></span></a></li>
                    <li><a className="nk-social-instagram" href="#"><span className="fab fa-instagram"></span></a></li>
                    <li><a className="nk-social-linkedin" href="#"><span className="fab fa-linkedin"></span></a></li>
                    <li><a className="nk-social-medium" href="#"><span className="fab fa-medium"></span></a></li>
                    <li><a className="nk-social-odnoklassniki" href="#"><span className="fab fa-odnoklassniki"></span></a></li>
                    <li><a className="nk-social-paypal" href="#"><span className="fab fa-p aypal"></span></a></li>
                    <li><a className="nk-social-reddit" href="#"><span className="fab fa-reddit"></span></a></li>
                    <li><a className="nk-social-skype" href="#"><span className="fab fa-skype"></span></a></li>
                    <li><a className="nk-social-soundcloud" href="#"><span className="fab fa-soundcloud"></span></a></li>
                    <li><a className="nk-social-slack" href="#"><span className="fab fa-slack"></span></a></li>
                    <li><a className="nk-social-tumblr" href="#"><span className="fab fa-tumblr"></span></a></li>
                    <li><a className="nk-social-vimeo" href="#"><span className="fab fa-vimeo"></span></a></li>
                    <li><a className="nk-social-vk" href="#"><span className="fab fa-vk"></span></a></li>
                    <li><a className="nk-social-wordpress" href="#"><span className="fab fa-wordpress"></span></a></li>
                    <li><a className="nk-social-youtube" href="#"><span className="fab fa-youtube"></span></a></li>
                -->*/}
            </ul>
          </div>
          <div className="col-3 nk-contacts-right">
            <ul className="nk-contacts-icons">
              {/* <li>
                <a href="#" data-toggle="modal" data-target="#modalSearch">
                  <span className="fa fa-search"></span>
                </a>
              </li> */}

              {/* <li>
                <a href="#" data-toggle="modal" data-target="#modalLogin">
                  <span className="fa fa-user"></span>
                </a>
              </li> */}
              {/* 
              <!-- <li>
                <span className="nk-cart-toggle">
                  <span className="fa fa-shopping-cart"></span>
                  <span className="nk-badge">27</span>
                </span>
                <div className="nk-cart-dropdown">
                  <div className="nk-widget-post">
                    <a href="store-product.html" className="nk-post-image">
                      <img
                        src="../src/assets/images/product-5-xs.jpg"
                        alt="In all revolutions of"
                      />
                    </a>
                    <h3 className="nk-post-title">
                      <a href="#" className="nk-cart-remove-item"
                        ><span className="ion-android-close"></span
                      ></a>
                      <a href="store-product.html">In all revolutions of</a>
                    </h3>
                    <div className="nk-gap-1"></div>
                    <div className="nk-product-price">€ 23.00</div>
                  </div>

                  <div className="nk-widget-post">
                    <a href="store-product.html" className="nk-post-image">
                      <img
                        src="../src/assets/images/product-7-xs.jpg"
                        alt="With what mingled joy"
                      />
                    </a>
                    <h3 className="nk-post-title">
                      <a href="#" className="nk-cart-remove-item"
                        ><span className="ion-android-close"></span
                      ></a>
                      <a href="store-product.html">With what mingled joy</a>
                    </h3>
                    <div className="nk-gap-1"></div>
                    <div className="nk-product-price">€ 14.00</div>
                  </div>

                  <div className="nk-gap-2"></div>
                  <div className="text-center">
                    <a
                      href="store-checkout.html"
                      className="
                        nk-btn
                        nk-btn-rounded
                        nk-btn-color-main-1
                        nk-btn-hover-color-white
                      "
                      >Proceed to Checkout</a
                    >
                  </div>
                </div>
              </li> --> */}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ContactsTop;
