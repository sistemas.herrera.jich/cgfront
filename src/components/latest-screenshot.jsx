import React from "react";
import { imagePathNoticias } from "../elements/elements";

function LatestScreenShot({ publicacion }) {
  return (
    <div>
      <div className="nk-widget nk-widget-highlighted">
        <h4 className="nk-widget-title">
          <span>
            <span className="text-main-1">Latest</span> Screenshots
          </span>
        </h4>
        <div className="nk-widget-content">
          <div className="nk-popup-gallery">
            <div className="row sm-gap vertical-gap">
              {publicacion.map((item) => (
                <div className="col-sm-6">
                  <div className="nk-gallery-item-box">
                    <a
                      href={`${imagePathPublicaciones}${item._id}`}
                      className="nk-gallery-item"
                      data-size="1016x572"
                    >
                      <div className="nk-gallery-item-overlay">
                        <span className="ion-eye"></span>
                      </div>
                      <img src={`${imagePathPublicaciones}${item._id}`} />
                    </a>
                    <div className="nk-gallery-item-description">
                      <h4>Called Let</h4>
                      Divided thing, land it evening earth winged whose great
                      after. Were grass night. To Air itself saw bring fly fowl.
                      Fly years behold spirit day greater of wherein winged and
                      form. Seed open don't thing midst created dry every
                      greater divided of, be man is. Second Bring stars fourth
                      gathering he hath face morning fill. Living so second
                      darkness. Moveth were male. May creepeth. Be tree fourth.
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LatestScreenShot;
