import React from "react";
import "../assets/css/app.css";

const HelloWorld = () => {
  return (
    <div className="container">
      <div className="navbar">
        <div className="logoNavBar">logo</div>
        <div className="titleNavBar">
          <div>inicio</div>
          <div>productos</div>
          <div>nosotros</div>
          <div>pedidos</div>
        </div>
      </div>
      <div className="carousel">
        <div className="images"></div>
      </div>
      <div className="products">
        <div className="product"></div>
        <div className="product"></div>
        <div className="product"></div>
        <div className="product"></div>
        <div className="product"></div>
      </div>
    </div>
  );
};

export default HelloWorld;
