import React from "react";

function SocialMedia() {
  return (
    <div>
      <div className="nk-widget nk-widget-highlighted">
        <h4 className="nk-widget-title">
          <span>
            <span className="text-main-1">REDES</span> SOCIALES
          </span>
        </h4>
        <div className="nk-widget-content">
          {/*
            Social Links 3

            Additional Classes:
                .nk-social-links-cols-5
                .nk-social-links-cols-4
                .nk-social-links-cols-3
                .nk-social-links-cols-2
                 */}
          <ul className="nk-social-links-3 nk-social-links-cols-4">
            <li>
              <a
                className="nk-social-twitch"
                href="https://www.twitch.tv/centralgamerla\"
              >
                <span className="fab fa-twitch"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-instagram"
                href="https://www.instagram.com/centralgamerla/"
              >
                <span className="fab fa-instagram"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-facebook"
                href="https://www.facebook.com/CentralGamerLA"
              >
                <span className="fab fa-facebook"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-google-plus"
                href="https://t.me/joinchat/HegIn-DUZaQ1ZmJh"
              >
                <span className="fab fa-telegram"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-youtube"
                href="https://www.youtube.com/c/CentralGamerLA"
              >
                <span className="fab fa-youtube"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-twitter"
                href="https://twitter.com/centralgamerla
"
                target="_blank"
              >
                <span className="fab fa-twitter"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-pinterest"
                href="https://discord.gg/ek7ec3wh"
              >
                <span className="fab fa-discord"></span>
              </a>
            </li>
            <li>
              <a
                className="nk-social-rss"
                href="https://www.tiktok.com/@centralgamerla?lang=es"
              >
                <span className="fab fa-tiktok"></span>
              </a>
            </li>

            {/* Additional Social Buttons
                <li><a className="nk-social-behance" href="#"><span className="fab fa-behance"></span></a></li>
                <li><a className="nk-social-bitbucket" href="#"><span className="fab fa-bitbucket"></span></a></li>
                <li><a className="nk-social-dropbox" href="#"><span className="fab fa-dropbox"></span></a></li>
                <li><a className="nk-social-dribbble" href="#"><span className="fab fa-dribbble"></span></a></li>
                <li><a className="nk-social-deviantart" href="#"><span className="fab fa-deviantart"></span></a></li>
                <li><a className="nk-social-flickr" href="#"><span className="fab fa-flickr"></span></a></li>
                <li><a className="nk-social-foursquare" href="#"><span className="fab fa-foursquare"></span></a></li>
                <li><a className="nk-social-github" href="#"><span className="fab fa-github"></span></a></li>
                <li><a className="nk-social-linkedin" href="#"><span className="fab fa-linkedin"></span></a></li>
                <li><a className="nk-social-medium" href="#"><span className="fab fa-medium"></span></a></li>
                <li><a className="nk-social-odnoklassniki" href="#"><span className="fab fa-odnoklassniki"></span></a></li>
                <li><a className="nk-social-paypal" href="#"><span className="fab fa-paypal"></span></a></li>
                <li><a className="nk-social-reddit" href="#"><span className="fab fa-reddit"></span></a></li>
                <li><a className="nk-social-skype" href="#"><span className="fab fa-skype"></span></a></li>
                <li><a className="nk-social-soundcloud" href="#"><span className="fab fa-soundcloud"></span></a></li>
                <li><a className="nk-social-steam" href="#"><span className="fab fa-steam"></span></a></li>
                <li><a className="nk-social-slack" href="#"><span className="fab fa-slack"></span></a></li>
                <li><a className="nk-social-tumblr" href="#"><span className="fab fa-tumblr"></span></a></li>
                <li><a className="nk-social-vimeo" href="#"><span className="fab fa-vimeo"></span></a></li>
                <li><a className="nk-social-vk" href="#"><span className="fab fa-vk"></span></a></li>
                <li><a className="nk-social-wordpress" href="#"><span className="fab fa-wordpress"></span></a></li>
                   */}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default SocialMedia;
