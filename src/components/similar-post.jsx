import React from "react";
import { Link } from "react-router-dom";
import { imagePathNoticias } from "../elements/elements";

function SimilarPost({ noticia }) {
  return (
    <div>
      {noticia[0] != null ? (
        <div>
          {" "}
          <div className="nk-gap-2"></div>
          <h3 className="nk-decorated-h-2">
            <span>
              <span className="text-main-1">Similar</span> Articles
            </span>
          </h3>
          <div className="nk-gap"></div>
          <div className="row">
            <div className="col-md-6">
              {/* <!-- START: Post --> */}
              <div className="nk-blog-post">
                <a
                  href={`/articulo/noticias/${noticia[0]._id}`}
                  className="nk-post-img"
                >
                  <img
                    src={`${imagePathNoticias}${noticia[0]._id}`}
                    alt="We found a witch! May we burn her?"
                  />
                  {/* <span className="nk-post-comments-count">7</span> */}

                  <span className="nk-post-categories">
                    {/* <span className="bg-main-2">
                      {noticia[0].categorias[0] != null
                        ? noticia[0].categorias[0]
                        : "Sin Categoria"}
                    </span> */}
                  </span>
                </a>
                <div className="nk-gap"></div>
                <h2 className="nk-post-title h4">
                  <a href={`/articulo/noticias/${noticia[0]._id}`}>
                    {noticia[0].titulo}
                  </a>
                </h2>
              </div>
              {/* <!-- END: Post --> */}
            </div>

            <div className="col-md-6">
              {/* <!-- START: Post --> */}
              <div className="nk-blog-post">
                <a
                  href={`/articulo/noticias/${noticia[1]._id}`}
                  className="nk-post-img"
                >
                  <img
                    src={`${imagePathNoticias}${noticia[1]._id}`}
                    alt="For good, too though, in consequence"
                  />
                  {/* <span className="nk-post-comments-count">23</span> */}

                  <span className="nk-post-categories">
                    {/* <span className="bg-main-3">
                      {" "}
                      {noticia[1].categorias[0] != null
                        ? noticia[1].categorias[0]
                        : "Sin Categoria"}
                    </span> */}
                  </span>
                </a>
                <div className="nk-gap"></div>
                <h2 className="nk-post-title h4">
                  <a href={`/articulo/noticias/${noticia[1]._id}`}>
                    {noticia[1].titulo}
                  </a>
                </h2>
              </div>
              {/* <!-- END: Post --> */}
            </div>
          </div>{" "}
        </div>
      ) : (
        ""
      )}
    </div>
  );
}

export default SimilarPost;
