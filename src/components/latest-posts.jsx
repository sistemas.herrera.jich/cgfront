import React from "react";
import { Link } from "react-router-dom";
import { formatearFecha, imagePathPublicaciones } from "../elements/elements";

function LatestPosts({ publicacion }) {
  return (
    <div>
      <h3 className="nk-decorated-h-2">
        <span>
          <span className="text-main-1">Últimas</span> Publicaciones
        </span>
      </h3>
      <div className="nk-gap"></div>
      {}
      <div className="nk-blog-grid">
        {publicacion[0] != null ? (
          <div className="row">
            <div className="col-md-6">
              {/* START: Post */}

              <div className="nk-blog-post">
                <a
                  href={`/articulo/publicaciones/${publicacion[0]._id}`}
                  className="nk-post-img"
                >
                  <img
                    src={`${imagePathPublicaciones}${publicacion[0]._id}`}
                    alt="He made his passenger captain of one"
                  />
                  {/* <span className="nk-post-comments-count">13</span> */}
                </a>
                <div className="nk-gap"></div>
                <h2 className="nk-post-title h4">
                  <Link
                    to={{
                      pathname: `/articulo/publicaciones/${publicacion[0]._id}`,
                    }}
                  >
                    {publicacion[0].titulo}
                  </Link>
                </h2>
                <div className="nk-post-by">
                  <img
                    src={`https://cgamer.herokuapp.com/usuarios/imagen/${publicacion[0].usuario._id}`}
                    alt="Wolfenstein"
                    className="rounded-circle"
                    width="35"
                  />
                  Por{" "}
                  <a href="#">
                    {publicacion[0].usuario != null
                      ? publicacion[0].usuario.nombre
                      : "unknow"}
                  </a>{" "}
                  el {formatearFecha(publicacion[0].created_at)}
                </div>
                <div className="nk-gap"></div>
                <div className="nk-post-text">
                  <p>{publicacion[0].descripcionCorta}</p>
                </div>
                <div className="nk-gap"></div>
                <a
                  href={`/articulo/publicaciones/${publicacion[0]._id}`}
                  className=" nk-btn nk-btn-rounded nk-btn-color-dark-3
                  nk-btn-hover-color-main-1 "
                >
                  LEER MÁS
                </a>
              </div>

              {/* END: Post */}
            </div>
            <div className="col-md-6">
              {/* START: Post */}

              <div className="nk-blog-post">
                <a
                  href={`/articulo/publicaciones/${publicacion[1]._id}`}
                  className="nk-post-img"
                >
                  <img
                    src={`${imagePathPublicaciones}${publicacion[1]._id}`}
                    alt="He made his passenger captain of one"
                  />
                  {/* <span className="nk-post-comments-count">13</span> */}
                </a>
                <div className="nk-gap"></div>
                <h2 className="nk-post-title h4">
                  <Link
                    to={{
                      pathname: `/articulo/publicaciones/${publicacion[1]._id}`,
                    }}
                  >
                    {publicacion[1].titulo}
                  </Link>
                </h2>
                <div className="nk-post-by">
                  <img
                    src={`https://cgamer.herokuapp.com/usuarios/imagen/${publicacion[1].usuario._id}`}
                    alt="Wolfenstein"
                    className="rounded-circle"
                    width="35"
                  />
                  Por{" "}
                  <a href="#">
                    {publicacion[0].usuario != null
                      ? publicacion[0].usuario.nombre
                      : "Sin Nombre"}
                  </a>{" "}
                  el {formatearFecha(publicacion[1].created_at)}
                </div>
                <div className="nk-gap"></div>
                <div className="nk-post-text">
                  <p>{publicacion[0].descripcionCorta}</p>
                </div>
                <div className="nk-gap"></div>
                <a
                  href={`/articulo/publicaciones/${publicacion[1]._id}`}
                  className=" nk-btn nk-btn-rounded nk-btn-color-dark-3
                  nk-btn-hover-color-main-1 "
                >
                  LEER MÁS
                </a>
              </div>

              {/* END: Post */}
            </div>
          </div>
        ) : (
          console.log("no hay data")
        )}
      </div>
    </div>
  );
}

export default LatestPosts;
