import React from "react";
import TabbedNews from "../components/tabbed-news";
import LatestPictures from "../components/latest-pictures";
import LatestScreenShot from "../components/latest-screenshot";
import LatestVideo from "../components/latest-video";
import LatestPosts from "../components/latest-posts";
import SocialMedia from "../components/social-media";
import Recent from "../components/recent";
import PostsNew from "../components/posts-new";
import { linkVideos } from "../elements/video";

function RowVertical({ publicacion, setNoticias, noticia, setNoticiasForce }) {
  return (
    <div className="container">
      <div className="row vertical-gap">
        <div className="col-lg-8">
          <LatestPosts publicacion={publicacion} />
          {/* END: Latest Posts */}

          {/* START: Latest Matches */}
          {/* <div className="nk-gap-2"></div>
            <h3 className="nk-decorated-h-2">
              <span><span className="text-main-1">Latest</span> Matches</span>
            </h3>
            <div className="nk-gap"></div>
            <div className="row">
              <div className="col-md-4">
                <div className="nk-match-score bg-dark-3">Now Playing</div>
                <div className="nk-gap-2"></div>
                <div className="nk-widget-match p-0">
                  <div className="nk-widget-match-teams">
                    <div className="nk-widget-match-team-logo">
                      <img src="assets/images/team-1.jpg" alt="" />
                    </div>
                    <div className="nk-widget-match-vs">VS</div>
                    <div className="nk-widget-match-team-logo">
                      <img src="assets/images/team-2.jpg" alt="" />
                    </div>
                  </div>
                </div>
                <div className="nk-gap-2"></div>
                <p>
                  As she said this she looked down at her hands and was
                  surprised to see.
                </p>
                <a
                  href="tournaments.html"
                  className="nk-btn nk-btn-rounded nk-btn-color-main-1"
                  >Match Details</a
                >
              </div>
              <div className="col-md-8">
                <div className="responsive-embed responsive-embed-16x9">
                  <iframe
                    src="https://player.twitch.tv/?channel=eulcs&autoplay=false"
                    frameborder="0"
                    allowfullscreen="true"
                    scrolling="no"
                    height="378"
                    width="620"
                  ></iframe>
                </div>
              </div>
            </div>
            <div className="nk-gap"></div>
            <div className="nk-match">
              <div className="nk-match-team-left">
                <a href="#">
                  <span className="nk-match-team-logo">
                    <img src="assets/images/team-1.jpg" alt="" />
                  </span>
                  <span className="nk-match-team-name"> SK Telecom T1 </span>
                </a>
              </div>
              <div className="nk-match-status">
                <a href="#">
                  <span className="nk-match-status-vs">VS</span>
                  <span className="nk-match-status-date">Apr 28, 2018 8:00 pm</span>
                  <span className="nk-match-score bg-danger"> 2 : 17 </span>
                </a>
              </div>
              <div className="nk-match-team-right">
                <a href="#">
                  <span className="nk-match-team-name"> Cloud 9 </span>
                  <span className="nk-match-team-logo">
                    <img src="assets/images/team-2.jpg" alt="" />
                  </span>
                </a>
              </div>
            </div>

            <div className="nk-match">
              <div className="nk-match-team-left">
                <a href="#">
                  <span className="nk-match-team-logo">
                    <img src="assets/images/team-3.jpg" alt="" />
                  </span>
                  <span className="nk-match-team-name"> Counted logic gaming </span>
                </a>
              </div>
              <div className="nk-match-status">
                <a href="#">
                  <span className="nk-match-status-vs">VS</span>
                  <span className="nk-match-status-date">Apr 15, 2018 9:00 pm</span>
                  <span className="nk-match-score bg-success"> 28 : 19 </span>
                </a>
              </div>
              <div className="nk-match-team-right">
                <a href="#">
                  <span className="nk-match-team-name"> SK Telecom T1 </span>
                  <span className="nk-match-team-logo">
                    <img src="assets/images/team-1.jpg" alt="" />
                  </span>
                </a>
              </div>
            </div>

            <div className="nk-match">
              <div className="nk-match-team-left">
                <a href="#">
                  <span className="nk-match-team-logo">
                    <img src="assets/images/team-4.jpg" alt="" />
                  </span>
                  <span className="nk-match-team-name"> Team SoloMid </span>
                </a>
              </div>
              <div className="nk-match-status">
                <a href="#">
                  <span className="nk-match-status-vs">VS</span>
                  <span className="nk-match-status-date">Apr 28, 2018 8:00 pm</span>
                  <span className="nk-match-score bg-dark-1"> 13 : 13 </span>
                </a>
              </div>
              <div className="nk-match-team-right">
                <a href="#">
                  <span className="nk-match-team-name"> SK Telecom T1 </span>
                  <span className="nk-match-team-logo">
                    <img src="assets/images/team-1.jpg" alt="" />
                  </span>
                </a>
              </div>
            </div> */}
          {/* END: Latest Matches */}
          <h3 className="nk-decorated-h-2">
            <span>
              <span className="text-main-1">NOTICIAS POR</span> CATEGORÍA
            </span>
          </h3>
          {noticia != null ? (
            <TabbedNews
              noticia={noticia}
              setNoticias={setNoticias}
              setNoticiasForce={setNoticiasForce}
              publicacion={publicacion}
            />
          ) : (
            ""
          )}

          {/* <LatestPictures /> */}

          {/* START: Best Selling */}
          {/* <div className="nk-gap-3"></div>
            <h3 className="nk-decorated-h-2">
              <span><span className="text-main-1">Best</span> Selling</span>
            </h3>
            <div className="nk-gap"></div>
            <div className="row vertical-gap">
              <div className="col-md-6">
                <div className="nk-product-cat">
                  <a className="nk-product-image" href="store-product.html">
                    <img
                      src="assets/images/product-11-xs.jpg"
                      alt="She gave my mother"
                    />
                  </a>
                  <div className="nk-product-cont">
                    <h3 className="nk-product-title h5">
                      <a href="store-product.html">She gave my mother</a>
                    </h3>
                    <div className="nk-gap-1"></div>
                    <div className="nk-product-rating" data-rating="3">
                      <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i> <i className="far fa-star"></i>
                      <i className="far fa-star"></i>
                    </div>
                    <div className="nk-gap-1"></div>
                    <div className="nk-product-price">€ 14.00</div>
                    <div className="nk-gap-1"></div>
                    <a
                      href="#"
                      className="
                        nk-btn
                        nk-btn-rounded
                        nk-btn-color-dark-3
                        nk-btn-hover-color-main-1
                      "
                      >Add to Cart</a
                    >
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <div className="nk-product-cat">
                  <a className="nk-product-image" href="store-product.html">
                    <img
                      src="assets/images/product-12-xs.jpg"
                      alt="A hundred thousand"
                    />
                  </a>
                  <div className="nk-product-cont">
                    <h3 className="nk-product-title h5">
                      <a href="store-product.html">A hundred thousand</a>
                    </h3>
                    <div className="nk-gap-1"></div>
                    <div className="nk-product-rating" data-rating="4.5">
                      <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                      <i className="fas fa-star-half"></i>
                    </div>
                    <div className="nk-gap-1"></div>
                    <div className="nk-product-price">€ 20.00</div>
                    <div className="nk-gap-1"></div>
                    <a
                      href="#"
                      className="
                        nk-btn
                        nk-btn-rounded
                        nk-btn-color-dark-3
                        nk-btn-hover-color-main-1
                      "
                      >Add to Cart</a
                    >
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <div className="nk-product-cat">
                  <a className="nk-product-image" href="store-product.html">
                    <img
                      src="assets/images/product-13-xs.jpg"
                      alt="So saying he unbuckled"
                    />
                  </a>
                  <div className="nk-product-cont">
                    <h3 className="nk-product-title h5">
                      <a href="store-product.html">So saying he unbuckled</a>
                    </h3>
                    <div className="nk-gap-1"></div>
                    <div className="nk-product-rating" data-rating="5">
                      <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i>
                    </div>
                    <div className="nk-gap-1"></div>
                    <div className="nk-product-price">€ 23.00</div>
                    <div className="nk-gap-1"></div>
                    <a
                      href="#"
                      className="
                        nk-btn
                        nk-btn-rounded
                        nk-btn-color-dark-3
                        nk-btn-hover-color-main-1
                      "
                      >Add to Cart</a
                    >
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <div className="nk-product-cat">
                  <a className="nk-product-image" href="store-product.html">
                    <img
                      src="assets/images/product-14-xs.jpg"
                      alt="However, I have reason"
                    />
                  </a>
                  <div className="nk-product-cont">
                    <h3 className="nk-product-title h5">
                      <a href="store-product.html">However, I have reason</a>
                    </h3>
                    <div className="nk-gap-1"></div>
                    <div className="nk-product-rating" data-rating="1.5">
                      <i className="fa fa-star"></i>
                      <i className="fas fa-star-half"></i>
                      <i className="far fa-star"></i> <i className="far fa-star"></i>
                      <i className="far fa-star"></i>
                    </div>
                    <div className="nk-gap-1"></div>
                    <div className="nk-product-price">€ 32.00</div>
                    <div className="nk-gap-1"></div>
                    <a
                      href="#"
                      className="
                        nk-btn
                        nk-btn-rounded
                        nk-btn-color-dark-3
                        nk-btn-hover-color-main-1
                      "
                      >Add to Cart</a
                    >
                  </div>
                </div>
              </div>
            </div> */}
          {/* END: Best Selling */}
        </div>
        <div className="col-lg-4">
          {/*
                START: Sidebar

                Additional Classes:
                    .nk-sidebar-left
                    .nk-sidebar-right
                    .nk-sidebar-sticky
            */}
          <aside className="nk-sidebar nk-sidebar-right nk-sidebar-sticky">
            {/* <div className="nk-widget">
              <div className="nk-widget-content">
                <form
                  action="#"
                  className="nk-form nk-form-style-1"
                  noValidate="novalidate"
                >
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Type something..."
                    />
                    <button className="nk-btn nk-btn-color-main-1">
                      <span className="ion-search"></span>
                    </button>
                  </div>
                </form>
              </div>
            </div> */}

            <SocialMedia />
            <div className="nk-gap-2"></div>
            <LatestVideo embedId={linkVideos} />
            {/* <div className="nk-gap-2"></div>
            <Recent /> */}
            {/* <LatestScreenShot publicacion={publicacion} /> */}
            {/* <div className="nk-widget nk-widget-highlighted">
                <h4 className="nk-widget-title">
                  <span><span className="text-main-1">Next</span> Matches</span>
                </h4>
                <div className="nk-widget-content">
                  <div className="nk-widget-match">
                    <a href="#">
                      <span className="nk-widget-match-left">
                        <span className="nk-widget-match-teams">
                          <span className="nk-widget-match-team-logo">
                            <img src="assets/images/team-1.jpg" alt="" />
                          </span>
                          <span className="nk-widget-match-vs">VS</span>
                          <span className="nk-widget-match-team-logo">
                            <img src="assets/images/team-2.jpg" alt="" />
                          </span>
                        </span>
                        <span className="nk-widget-match-date"
                          >CS:GO - Apr 28, 2018 8:00 pm</span
                        >
                      </span>
                      <span className="nk-widget-match-right">
                        <span className="nk-match-score"> Upcoming </span>
                      </span>
                    </a>
                  </div>

                  <div className="nk-widget-match">
                    <a href="#">
                      <span className="nk-widget-match-left">
                        <span className="nk-widget-match-teams">
                          <span className="nk-widget-match-team-logo">
                            <img src="assets/images/team-3.jpg" alt="" />
                          </span>
                          <span className="nk-widget-match-vs">VS</span>
                          <span className="nk-widget-match-team-logo">
                            <img src="assets/images/team-2.jpg" alt="" />
                          </span>
                        </span>
                        <span className="nk-widget-match-date"
                          >LoL - Apr 24, 2018 7:20 pm</span
                        >
                      </span>
                      <span className="nk-widget-match-right">
                        <span className="nk-match-score"> Upcoming </span>
                      </span>
                    </a>
                  </div>

                  <div className="nk-widget-match">
                    <a href="#">
                      <span className="nk-widget-match-left">
                        <span className="nk-widget-match-teams">
                          <span className="nk-widget-match-team-logo">
                            <img src="assets/images/team-1.jpg" alt="" />
                          </span>
                          <span className="nk-widget-match-vs">VS</span>
                          <span className="nk-widget-match-team-logo">
                            <img src="assets/images/team-4.jpg" alt="" />
                          </span>
                        </span>
                        <span className="nk-widget-match-date"
                          >Dota 2 - Apr 12, 2018 6:40 pm</span
                        >
                      </span>
                      <span className="nk-widget-match-right">
                        <span className="nk-match-score bg-dark-1"> 0 : 0 </span>
                      </span>
                    </a>
                  </div>
                </div>
              </div> */}
            {/* <div className="nk-widget nk-widget-highlighted">
                <h4 className="nk-widget-title">
                  <span><span className="text-main-1">Most</span> Popular</span>
                </h4>
                <div className="nk-widget-content">
                  <div className="nk-widget-post">
                    <a href="store-product.html" className="nk-post-image">
                      <img
                        src="assets/images/product-1-xs.jpg"
                        alt="So saying he unbuckled"
                      />
                    </a>
                    <h3 className="nk-post-title">
                      <a href="store-product.html">So saying he unbuckled</a>
                    </h3>
                    <div className="nk-product-rating" data-rating="4">
                      <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                      <i className="far fa-star"></i>
                    </div>
                    <div className="nk-product-price">€ 23.00</div>
                  </div>

                  <div className="nk-widget-post">
                    <a href="store-product.html" className="nk-post-image">
                      <img
                        src="assets/images/product-2-xs.jpg"
                        alt="However, I have reason"
                      />
                    </a>
                    <h3 className="nk-post-title">
                      <a href="store-product.html">However, I have reason</a>
                    </h3>
                    <div className="nk-product-rating" data-rating="2.5">
                      <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                      <i className="fas fa-star-half"></i>
                      <i className="far fa-star"></i> <i className="far fa-star"></i>
                    </div>
                    <div className="nk-product-price">€ 32.00</div>
                  </div>

                  <div className="nk-widget-post">
                    <a href="store-product.html" className="nk-post-image">
                      <img
                        src="assets/images/product-3-xs.jpg"
                        alt="It was some time before"
                      />
                    </a>
                    <h3 className="nk-post-title">
                      <a href="store-product.html">It was some time before</a>
                    </h3>
                    <div className="nk-product-rating" data-rating="5">
                      <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i>
                    </div>
                    <div className="nk-product-price">€ 14.00</div>
                  </div>
                </div>
              </div> */}
          </aside>
          {/* END: Sidebar */}
        </div>
      </div>
    </div>
  );
}

export default RowVertical;
