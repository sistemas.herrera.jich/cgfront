import React, { useState, useEffect } from "react";
import logo from "../assets/images/logoCG.png";
import "../assets/css/app.css";
import "../assets/css/navbar.css";

function NavBar() {
  useEffect(() => {
    const btnMobile = document.getElementById("btn-mobile");
    btnMobile.addEventListener("click", toggleMenu);
    btnMobile.addEventListener("touchstart", toggleMenu);
  }, []);

  const [navbar, setNavbar] = useState(true);
  function toggleMenu(event) {
    if (event.type === "touchstart") event.preventDefault();
    const nav = document.getElementById("nav");
    nav.classList.toggle("active");
    const active = nav.classList.contains("active");
    event.currentTarget.setAttribute("aria-expanded", active);
    if (active) {
      event.currentTarget.setAttribute("aria-label", "Fechar Menu");
    } else {
      event.currentTarget.setAttribute("aria-label", "Abrir Menu");
    }
  }

  return (
    <div className="navcontainer">
      <div id="header" className="container">
        <a id="logo" href="/">
          <img style={{ height: "70px" }} src={logo} alt="" />
        </a>
        <nav id="nav">
          <button
            aria-label="Abrir Menu"
            id="btn-mobile"
            aria-haspopup="true"
            aria-controls="menu"
            aria-expanded="false"
            style={{ color: "white" }}
          >
            Menu
            <span id="hamburger"></span>
          </button>
          <ul id="menu" role="menu">
            <li>
              <a
                style={{ color: "white" }}
                href="http://192.168.3.45:3005/noticias/1"
              >
                Noticias
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
}

export default NavBar;
