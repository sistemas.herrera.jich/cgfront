import React, { useEffect, useState } from "react";
import { formatearFecha, imagePathNoticias, Path } from "../elements/elements";
import Loader from "react-loader";

function TabbedNews({ noticia, setNoticias, setNoticiasForce }) {
  const [bloqueo, setBloqueo] = useState({ bloque: false, valor: "Accion" });
  const filtrarNoticias = async (value) => {
    setBloqueo({ bloque: true });
    setNoticias(`${Path}/noticias?categorias=${value}`);
    return setBloqueo({ bloque: false, valor: value });
  };
  return (
    <div style={{ minHeight: "50vh" }}>
      {/* START: Tabbed News  */}

      <div className="nk-gap"></div>
      <div className="nk-tabs">
        {/*
                    Additional Classes:
                        .nav-tabs-fill
                */}
        <ul className="nav nav-tabs nav-tabs-fill" role="tablist">
          <li className="nav-item">
            <a
              className={
                bloqueo.valor == "Accion" ? "nav-link active" : "nav-link"
              }
              href="#tabs-1-1"
              role="tab"
              data-toggle="tab"
              onClick={() =>
                bloqueo.bloque == false && bloqueo.valor != "Accion"
                  ? filtrarNoticias("Accion")
                  : ""
              }
            >
              Accion
            </a>
          </li>
          <li className="nav-item">
            <a
              className={
                bloqueo.valor == "Aventura" ? "nav-link active" : "nav-link"
              }
              href="#tabs-1-2"
              role="tab"
              data-toggle="tab"
              onClick={() =>
                bloqueo.bloque == false && bloqueo.valor != "Aventura"
                  ? filtrarNoticias("Aventura")
                  : ""
              }
            >
              Aventura
            </a>
          </li>
          <li className="nav-item">
            <a
              className={
                bloqueo.valor == "MMO" ? "nav-link active" : "nav-link"
              }
              href="#tabs-1-3"
              role="tab"
              data-toggle="tab"
              onClick={() =>
                bloqueo.bloque == false && bloqueo.valor != "MMO"
                  ? filtrarNoticias("MMO")
                  : ""
              }
            >
              MMO
            </a>
          </li>
          <li className="nav-item">
            <a
              className={
                bloqueo.valor == "RPG" ? "nav-link active" : "nav-link"
              }
              href="#tabs-1-4"
              role="tab"
              data-toggle="tab"
              onClick={() =>
                bloqueo.bloque == false && bloqueo.valor != "RPG"
                  ? filtrarNoticias("RPG")
                  : ""
              }
            >
              RPG
            </a>
          </li>
          <li className="nav-item">
            <a
              className={
                bloqueo.valor == "Fantasia" ? "nav-link active" : "nav-link"
              }
              href="#tabs-1-5"
              role="tab"
              data-toggle="tab"
              onClick={() =>
                bloqueo.bloque == false && bloqueo.valor != "Fantasia"
                  ? filtrarNoticias("Fantasia")
                  : ""
              }
            >
              Fantasía
            </a>
          </li>
          <li className="nav-item">
            <a
              className={
                bloqueo.valor == "Terror" ? "nav-link active" : "nav-link"
              }
              href="#tabs-1-6"
              role="tab"
              data-toggle="tab"
              onClick={() =>
                bloqueo.bloque == false && bloqueo.valor != "Terror"
                  ? filtrarNoticias("Terror")
                  : ""
              }
            >
              Terror
            </a>
          </li>
        </ul>
        <div className="tab-content">
          <div
            role="tabpanel"
            className="tab-pane fade show active"
            id="tabs-1-1"
          >
            <div className="nk-gap"></div>
            {/* START: Action Tab */}
            {noticia != null ? (
              noticia.map((item) => (
                <div
                  className="nk-blog-post nk-blog-post-border-bottom"
                  key={item._id}
                >
                  <div className="row vertical-gap">
                    <div className="col-lg-3 col-md-5">
                      <a
                        href={`/articulo/noticias/${item._id}`}
                        className="nk-post-img"
                      >
                        <img
                          src={`${imagePathNoticias}${item._id}`}
                          alt="For good, too though, in consequence"
                        />

                        <span className="nk-post-categories">
                          <span className="bg-main-5">
                            {item.categorias[0] != null
                              ? item.categorias[0].nombre
                              : "Sin Categoria"}
                          </span>
                        </span>
                      </a>
                    </div>
                    <div className="col-lg-9 col-md-7">
                      <h2 className="nk-post-title h4">
                        <a href={`/articulo/noticias/${item._id}`}>
                          {item.titulo}
                        </a>
                      </h2>
                      <div className="nk-post-date mt-10 mb-10">
                        <span className="fa fa-calendar"></span>{" "}
                        {formatearFecha(item.created_at)}
                        {/* <span className="fa fa-comments"></span> */}
                        {/* <a href="#">23 comments</a> */}
                      </div>
                      <div className="nk-post-text">
                        <p>{item.descripcionCorta}</p>
                      </div>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <div>
                <Loader
                  style={{ marginTop: "50vh" }}
                  loaded={false}
                  lines={13}
                  length={20}
                  width={5}
                  radius={20}
                  corners={1}
                  rotate={0}
                  direction={1}
                  color="white"
                  speed={1}
                  trail={60}
                  shadow={false}
                  hwaccel={false}
                  className="spinner"
                  zIndex={2e9}
                  top="85%"
                  left="50%"
                  scale={0.7}
                  loadedClassName="loadedContent"
                />
              </div>
            )}

            {/* END: Action Tab */}
            <div className="nk-gap"></div>
          </div>
        </div>
      </div>
      {/* END: Tabbed News */}
    </div>
  );
}

export default TabbedNews;
