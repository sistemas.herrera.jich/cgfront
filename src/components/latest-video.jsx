import React from "react";

function LatestVideo({ embedId }) {
  return (
    <div>
      <div className="nk-widget nk-widget-highlighted">
        <h4 className="nk-widget-title">
          <span>
            <span className="text-main-1">Último</span> Video
          </span>
        </h4>
        <div className="nk-widget-content">
          <div className="nk-plain-video">
            <iframe
              width="ine"
              height="auto"
              src={`https://www.youtube.com/embed/${embedId}`}
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
              title="Embedded youtube"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default LatestVideo;
