import React, { useEffect, useState, useContext } from "react";
import img from "../assets/images/post-1.jpg";
import Slider, { nextButton } from "react-animated-slider";
import img2 from "../assets/images/iconPrueba.svg";
import "../assets/css/slider-animations.css";
import { imagePathDestacados } from "../elements/elements";

function slider({ articulo, setArticulos }) {
  return (
    <div className="container">
      {articulo != null ? (
        <Slider infinite={true} autoplay={5000}>
          {articulo.map((item, index) => (
            <div
              key={index}
              style={{
                background: `url('${imagePathDestacados}${item._id}') no-repeat center center`,
              }}
            >
              <div className="center">
                <div style={{ textAlign: "center" }}>
                  <h1>{item.titulo}</h1>
                </div>
                <div>
                  <p>{item.descripcion}</p>
                </div>
                <div>
                  <a
                    href={item.link}
                    style={{
                      textDecoration: "none",
                      backgroundColor: "#b46aeb",
                    }}
                    className="button"
                  >
                    LEER MÁS
                  </a>
                </div>
              </div>
            </div>
          ))}
        </Slider>
      ) : (
        ""
      )}
    </div>
    // <div className="nk-image-slider">
    //   <div className="nk-image-slider-item">
    //     <img
    //       src="assets/images/slide-1.jpg"
    //       alt=""
    //       className="nk-image-slider-img"
    //       data-thumb="assets/images/slide-1-thumb.jpg"
    //     />

    //     <div className="nk-image-slider-content">
    //       <h3 className="h4">As we Passed, I remarked</h3>
    //       <p className="text-white">
    //         As we passed, I remarked a beautiful church-spire rising above some
    //         old elms in the park; and before them, in the midst of a lawn,
    //         chimneys covered with ivy, and the windows shining in the sun.
    //       </p>
    //       <a
    //         href="#"
    //         className="
    //               nk-btn
    //               nk-btn-rounded
    //               nk-btn-color-white
    //               nk-btn-hover-color-main-1
    //             "
    //       >
    //         LEER MÁS
    //       </a>
    //     </div>
    //   </div>
    // </div>
  );
}

export default slider;
