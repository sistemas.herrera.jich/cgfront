import React from "react";

function LatestPictures() {
  return (
    <div>
      {/* START: Latest Pictures */}
      <div className="nk-gap"></div>
      <h2 className="nk-decorated-h-2 h3">
        <span>
          <span className="text-main-1">Latest</span> Pictures
        </span>
      </h2>
      <div className="nk-gap"></div>
      <div className="nk-popup-gallery">
        <div className="row vertical-gap">
          <div className="col-lg-4 col-md-6">
            <div className="nk-gallery-item-box">
              <a
                href="assets/images/gallery-1.jpg"
                className="nk-gallery-item"
                data-size="1016x572"
              >
                <div className="nk-gallery-item-overlay">
                  <span className="ion-eye"></span>
                </div>
                <img src="assets/images/gallery-1-thumb.jpg" alt="" />
              </a>
              <div className="nk-gallery-item-description">
                <h4>Called Let</h4>
                Divided thing, land it evening earth winged whose great after.
                Were grass night. To Air itself saw bring fly fowl. Fly years
                behold spirit day greater of wherein winged and form. Seed open
                don't thing midst created dry every greater divided of, be man
                is. Second Bring stars fourth gathering he hath face morning
                fill. Living so second darkness. Moveth were male. May creepeth.
                Be tree fourth.
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="nk-gallery-item-box">
              <a
                href="assets/images/gallery-2.jpg"
                className="nk-gallery-item"
                data-size="1188x594"
              >
                <div className="nk-gallery-item-overlay">
                  <span className="ion-eye"></span>
                </div>
                <img src="assets/images/gallery-2-thumb.jpg" alt="" />
              </a>
              <div className="nk-gallery-item-description">
                Seed open don't thing midst created dry every greater divided
                of, be man is. Second Bring stars fourth gathering he hath face
                morning fill. Living so second darkness. Moveth were male. May
                creepeth. Be tree fourth.
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="nk-gallery-item-box">
              <a
                href="assets/images/gallery-3.jpg"
                className="nk-gallery-item"
                data-size="625x350"
              >
                <div className="nk-gallery-item-overlay">
                  <span className="ion-eye"></span>
                </div>
                <img src="assets/images/gallery-3-thumb.jpg" alt="" />
              </a>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="nk-gallery-item-box">
              <a
                href="assets/images/gallery-4.jpg"
                className="nk-gallery-item"
                data-size="873x567"
              >
                <div className="nk-gallery-item-overlay">
                  <span className="ion-eye"></span>
                </div>
                <img src="assets/images/gallery-4-thumb.jpg" alt="" />
              </a>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="nk-gallery-item-box">
              <a
                href="assets/images/gallery-5.jpg"
                className="nk-gallery-item"
                data-size="471x269"
              >
                <div className="nk-gallery-item-overlay">
                  <span className="ion-eye"></span>
                </div>
                <img src="assets/images/gallery-5-thumb.jpg" alt="" />
              </a>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="nk-gallery-item-box">
              <a
                href="assets/images/gallery-6.jpg"
                className="nk-gallery-item"
                data-size="472x438"
              >
                <div className="nk-gallery-item-overlay">
                  <span className="ion-eye"></span>
                </div>
                <img src="assets/images/gallery-6-thumb.jpg" alt="" />
              </a>
            </div>
          </div>
        </div>
      </div>
      {/* END: Latest Pictures */}
    </div>
  );
}

export default LatestPictures;
