import React from "react";
import { Link, useParams } from "react-router-dom";
import { imagePathNoticias } from "../elements/elements";

function PostsNew({ setPublicaciones, noticias, pagina }) {
  const pagination = Math.ceil(pagina.length / 10);
  const { id } = useParams();
  return (
    <div>
      <div className="nk-blog-grid">
        <div className="row" style={{ minHeight: "120vh" }}>
          {noticias.map((item) => (
            <div className="col-md-6" key={item._id}>
              {/* START: Post */}
              <div className="nk-blog-post">
                <a
                  href={`/articulo/noticias/${item._id}`}
                  className="nk-post-img"
                >
                  <img
                    src={`${imagePathNoticias}${item._id}`}
                    alt="Smell magic in the air. Or maybe barbecue"
                  />
                  {/* <span className="nk-post-comments-count">4</span> */}
                  <span className="nk-post-categories">
                    <span className="bg-main-4">
                      {item.categorias[0] != null
                        ? item.categorias[0].nombre
                        : ""}
                    </span>
                  </span>
                </a>
                <div className="nk-gap"></div>
                <h2 className="nk-post-title h4">
                  <a href={`/articulo/noticias/${item._id}`}>{item.titulo}</a>
                </h2>
                <div className="nk-post-text">
                  <p>{item.descripcionCorta}</p>
                </div>
                <div className="nk-gap"></div>

                <Link
                  replace
                  className="
                                nk-btn
                                nk-btn-rounded
                                nk-btn-color-dark-3
                                nk-btn-hover-color-main-1
                            "
                  to={{
                    pathname: `/articulo/noticias/${item._id}`,
                  }}
                >
                  LEER MÁS
                </Link>
                <div className="nk-post-date float-right">
                  <span className="fa fa-calendar"></span> Sep 18, 2018
                </div>
              </div>
              {/* END: Post */}
            </div>
          ))}
        </div>

        {/* START: Pagination */}
        <div className="nk-pagination nk-pagination-center">
          <a href="#" className="nk-pagination-prev">
            <span className="ion-ios-arrow-back"></span>
          </a>
          <nav style={{ position: "relative" }}>
            {Number(id) > 0 && Number(id) - 2 > 0 ? (
              <a href={`/noticias/${Number(id) - 2}`} className="nk-pagination">
                {Number(id) - 2}
              </a>
            ) : (
              ""
            )}
            {Number(id) > 0 && Number(id) - 1 > 0 ? (
              <a href={`/noticias/${Number(id) - 1}`} className="nk-pagination">
                {Number(id) - 1}
              </a>
            ) : (
              ""
            )}
            <a className="nk-pagination-current">{id}</a>
            {Number(id) < pagination ? (
              <a href={`/noticias/${Number(id) + 1}`} className="nk-pagination">
                {Number(id) + 1}
              </a>
            ) : (
              ""
            )}
            {Number(id) + 1 < pagination ? (
              <a href={`/noticias/${Number(id) + 2}`} className="nk-pagination">
                {Number(id) + 2}
              </a>
            ) : (
              ""
            )}

            <span>...</span>
            <a href={`/noticias/${pagination}`}>{pagination}</a>
          </nav>
          <a href="#" className="nk-pagination-next">
            <span className="ion-ios-arrow-forward"></span>
          </a>
        </div>
        {/* END: Pagination */}
      </div>
      {/* END: Posts Grid */}
    </div>
  );
}

export default PostsNew;
